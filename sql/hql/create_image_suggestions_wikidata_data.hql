-- Usage:
--     spark3-sql -f create_image_suggestions_wikidata_data.hql \
--                --database analytics_platform_eng


CREATE TABLE `image_suggestions_wikidata_data`(
    `page_id` bigint COMMENT 'Uniquely identifying primary key along with wiki. This value is preserved across edits, renames, and, as of MediaWiki 1.27, deletions, via an analogous field in the archive table (introduced in MediaWiki 1.11). For example, for this page, page_id = 10501. ',
    `item_id` string COMMENT 'The id of the entity, P31 or Q32753077 for instance',
    `tag` string,
    `score` int)
PARTITIONED BY (
    `snapshot` string)
STORED AS PARQUET
;