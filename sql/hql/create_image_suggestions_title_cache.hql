-- Usage:
--     spark3-sql -f create_image_suggestions_title_cache.hql \
--                --database analytics_platform_eng


CREATE TABLE `image_suggestions_title_cache`(
    `page_id` bigint COMMENT 'Uniquely identifying primary key along with wiki. This value is preserved across edits, renames, and, as of MediaWiki 1.27, deletions, via an analogous field in the archive table (introduced in MediaWiki 1.11). For example, for this page, page_id = 10501. ',
    `page_rev` bigint,
    `title` string COMMENT 'The sanitized page title, without the namespace, with a maximum of 255 characters (binary). It is stored as text, with spaces replaced by underscores. The real title shown in articles is just this title with underscores (_) converted to spaces ( ). For exa')
PARTITIONED BY (
    `snapshot` string,
    `wiki` string COMMENT 'The wiki_db project')
STORED AS PARQUET
;