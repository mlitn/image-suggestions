#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from conftest import assert_shallow_equals
from pyspark.sql import functions as F
from pyspark.sql.types import NullType
from image_suggestions import cassandra, shared


def test_get_images_with_disallowed_substrings(spark_session, mocker):
    short_snapshot = '2022-01'
    mocker.patch(
        'image_suggestions.cassandra.load_commons_images',
        return_value=spark_session.createDataFrame(
            [
                (111, 'page_111_flag'),
                (222, 'page_222_disambig'),
                (333, 'page_333_default'),
                (444, 'page_444'),
            ],
            ['page_id', 'page_title']
        )
    )
    expected = spark_session.createDataFrame(
        [
            (111, 'page_111_flag'),
            (222, 'page_222_disambig'),
            (333, 'page_333_default'),
        ],
        ['page_id', 'page_title']
    )
    actual = cassandra.get_images_with_disallowed_substrings(short_snapshot)
    assert_shallow_equals(actual, expected)

def test_get_link_thresholds_per_wiki(spark_session, mocker):
    mocker.patch(
        'image_suggestions.cassandra.load_wiki_sizes',
        return_value=spark_session.createDataFrame(
            [
                ('awiki', 1),
                ('bwiki', 4000),
                ('cwiki', 8000),
                ('dwiki', 12000),
                ('ewiki', 16000),
                ('fwiki', 20000),
                ('gwiki', 30000),
                ('hwiki', 40000),
                ('iwiki', 50000),
                ('jwiki', 60000),
                ('kwiki', 70000),
                ('lwiki', 80000),
                ('mwiki', 90000),
                ('nwiki', 100000),
            ],
            ['wiki_db', 'size']
        )
    )
    expected = spark_session.createDataFrame(
        [
            ('awiki', 1, 4),
            ('bwiki', 4000, 4),
            ('cwiki', 8000, 4),
            ('dwiki', 12000, 4),
            ('ewiki', 16000, 5),
            ('fwiki', 20000, 6),
            ('gwiki', 30000, 9),
            ('hwiki', 40000, 12),
            ('iwiki', 50000, 15),
            ('jwiki', 60000, 17),
            ('kwiki', 70000, 18),
            ('lwiki', 80000, 19),
            ('mwiki', 90000, 19),
            ('nwiki', 100000, 20),
        ],
        ['wiki_db', 'size', 'threshold']
    )
    actual = cassandra.get_link_thresholds_per_wiki('2022-01')
    assert_shallow_equals(actual, expected)

def test_get_images_with_too_many_links(spark_session, mocker):
    mocker.patch(
        'image_suggestions.cassandra.get_wiktionaries',
        return_value=['cwiktionary']
    )
    mocker.patch(
        'image_suggestions.cassandra.get_link_thresholds_per_wiki',
        return_value=spark_session.createDataFrame(
            [
                ('awiki', 1, 4),
                ('bwiki', 20000, 6),
                ('cwiktionary', 1, 4),
            ],
            ['wiki_db', 'size', 'threshold']
        )
    )
    mocker.patch(
        'image_suggestions.cassandra.load_imagelinks',
        return_value=spark_session.createDataFrame(
            [
                # fewer links than threshold in all wikis - not expected in output
                ('awiki', 1, 'image_title_1'),
                ('awiki', 2, 'image_title_1'),
                ('awiki', 3, 'image_title_1'),
                ('bwiki', 1, 'image_title_1'),
                ('bwiki', 2, 'image_title_1'),
                ('bwiki', 3, 'image_title_1'),
                ('bwiki', 4, 'image_title_1'),
                ('bwiki', 5, 'image_title_1'),
                # more links than threshold in any wiki - expected in output
                ('awiki', 10, 'image_title_2'),
                ('awiki', 11, 'image_title_2'),
                ('awiki', 12, 'image_title_2'),
                ('awiki', 13, 'image_title_2'),
                ('awiki', 14, 'image_title_2'),
                # fewer links than threshold ONLY in a wikitionary - not expected in output
                ('cwiktionary', 1000, 'image_title_3'),
                # fewer links than threshold in a wikitionary, but more in a wiki - expected in output
                ('cwiktionary', 200, 'image_title_4'),
                ('awiki', 201, 'image_title_4'),
                ('awiki', 202, 'image_title_4'),
                ('awiki', 203, 'image_title_4'),
                ('awiki', 204, 'image_title_4'),
                ('awiki', 205, 'image_title_4'),
            ],
            ['wiki_db', 'article_id', 'image_title']
        )
    )
    mocker.patch(
        'image_suggestions.cassandra.load_commons_images',
        return_value=spark_session.createDataFrame(
            [
                (100, 'image_title_1'),
                (101, 'image_title_2'),
                (102, 'image_title_3'),
                (103, 'image_title_4'),
            ],
            ['page_id', 'page_title']
        )
    )
    expected = spark_session.createDataFrame(
        [
            (101, 'image_title_2'),
            (103, 'image_title_4'),
        ],
        ['page_id', 'page_title']
    )

    actual = cassandra.get_images_with_too_many_links('2022-01')
    assert_shallow_equals(actual, expected)

def test_get_all_illustrations(spark_session, mocker):
    short_snapshot = '2022-01'
    mocker.patch(
        'image_suggestions.cassandra.load_commons_images',
        return_value=spark_session.createDataFrame(
            [
                (111, 'page_111'),
                (222, 'page_222'),
                (333, 'page_333'),
                (444, 'page_444'),
            ],
            ['page_id', 'page_title']
        )
    )
    mocker.patch(
        'image_suggestions.cassandra.load_images_in_placeholder_categories',
        return_value=spark_session.createDataFrame([{'page_title': 'page_111'}])
    )
    mocker.patch(
        'image_suggestions.cassandra.get_images_with_too_many_links',
        return_value=spark_session.createDataFrame(
            [(222, 'page_222'),],
            ['page_id', 'page_title']
        )
    )
    mocker.patch(
        'image_suggestions.cassandra.get_images_with_disallowed_substrings',
        return_value=spark_session.createDataFrame(
            [(333, 'page_333'),],
            ['page_id', 'page_title']
        )
    )
    expected = spark_session.createDataFrame(
       [(444, 'page_444'),],
       ['page_id', 'page_title']
    )
    assert_shallow_equals(cassandra.get_all_illustrations(short_snapshot), expected)

def test_get_unillustrated_articles(spark_session, mocker):
    short_snapshot = '2022-01'
    mocker.patch(
        'image_suggestions.cassandra.load_commons_images',
        return_value=spark_session.createDataFrame(
            [
                (1, 'commons_page_1'),
                (2, 'commons_page_icon'),
            ],
            ['page_id', 'page_title']
        )
    )
    mocker.patch(
        'image_suggestions.cassandra.get_images_with_too_many_links',
        return_value=spark_session.createDataFrame(
            [
                (2, 'commons_page_icon'),
            ],
            ['page_id', 'page_title']
        )
    )
    mocker.patch(
        'image_suggestions.cassandra.load_local_images',
        return_value=spark_session.createDataFrame(
            [
                ('dwiki', 10, 'local_file_1'),
            ],
            ['wiki_db', 'page_id', 'page_title']
        )
    )
    mocker.patch(
        'image_suggestions.cassandra.load_imagelinks',
        return_value=spark_session.createDataFrame(
            [
                ('awiki', 111, 'commons_page_1'),
                ('bwiki', 222, 'commons_page_icon'),
                ('bwiki', 333, 'commons_page_icon'),
                ('bwiki', 333, 'commons_page_1'),
                ('dwiki', 444, 'local_file_1'),
            ],
            ['wiki_db', 'article_id', 'image_title']
        )
    )
    mocker.patch(
        'image_suggestions.cassandra.load_non_commons_main_pages',
        return_value=spark_session.createDataFrame(
            [
                # linked to 'commons_page_1' so should NOT be returned
                ('awiki', 111, 'wiki_page_111'),
                # linked ONLY to an image with too many links, so should be returned
                ('bwiki', 222, 'wiki_page_222'),
                # linked to an image with too many links but also another image, so should NOT be returned
                ('bwiki', 333, 'wiki_page_333'),
                ('bwiki', 333, 'wiki_page_333'),
                # no imagelink, so should be returned
                ('cwiki', 444, 'wiki_page_444'),
                # imagelink to local_file_1, should NOT be returned
                ('dwiki', 444, 'wiki_page_444'),
            ],
            ['wiki_db', 'page_id', 'page_title']
        )
    )
    expected = spark_session.createDataFrame(
        [
            ('bwiki', 222, 'wiki_page_222'),
            ('cwiki', 444, 'wiki_page_444'),
        ],
        ['wiki_db', 'page_id', 'page_title']
    )
    actual = cassandra.get_unillustrated_articles(short_snapshot)
    assert_shallow_equals(actual, expected)

def test_get_non_illustratable_item_ids(spark_session, mocker):
    mocker.patch(
        'image_suggestions.cassandra.load_wikidata_items_with_P31',
        return_value=spark_session.createDataFrame(
            [
                ('Q1', 'Q577'),
                ('Q2', 'Q42'),
                ('Q3', 'Q4167410'),
            ],
            [ 'item_id', 'value' ]
        )
    )
    expected = spark_session.createDataFrame([{"item_id":"Q1"}, {"item_id":"Q3"}])
    actual = cassandra.get_non_illustratable_item_ids('2022-01-01')
    assert_shallow_equals(actual, expected)



def test_get_illustratable_articles(spark_session, mocker):
    mocker.patch(
    'image_suggestions.cassandra.get_unillustrated_articles',
        return_value=spark_session.createDataFrame(
            [
                ('awiki', 111, 'wiki_page_111'),
                ('bwiki', 222, 'wiki_page_222'),
                ('cwiki', 333, 'wiki_page_333'),
                ('cwiki', 111, 'wiki_page_111'),
                ('cwiki', 999, 'article_with_no_item_id'),
            ],
            ['wiki_db', 'page_id', 'page_title']
        )
    )
    mocker.patch(
        'image_suggestions.cassandra.load_wikidata_item_page_links',
        return_value=spark_session.createDataFrame(
            [
                ('awiki', 111, 'Q1'),
                ('bwiki', 222, 'Q2'),
                ('cwiki', 333, 'Q3'),
                ('cwiki', 111, 'Q5'),
            ],
            ['wiki_db', 'page_id', 'item_id']
        )
    )
    mocker.patch(
        'image_suggestions.cassandra.get_non_illustratable_item_ids',
        return_value=spark_session.createDataFrame(
            [
                {"item_id":"Q1"},
                {"item_id":"Q3"},
                {"item_id":"Q4"},
            ]
        )
    )
    expected = spark_session.createDataFrame(
        [
            ('bwiki', 222, 'wiki_page_222', 'Q2'),
            ('cwiki', 111, 'wiki_page_111', 'Q5'),
        ],
        ['wiki_db', 'page_id', 'page_title', 'item_id']
    )
    actual = cassandra.get_illustratable_articles('2022-01-01')
    assert_shallow_equals(actual, expected)

def test_get_illustratable_articles_with_revisions(spark_session, mocker):
    mocker.patch(
        'image_suggestions.cassandra.get_illustratable_articles',
        return_value=spark_session.createDataFrame(
            [
                ('bwiki', 222, 'article_title_1', 'Q2'),
                ('cwiki', 111, 'article_title_2', 'Q5'),
                # article without latest revision (shouldn't happen, but test just in case)
                ('ewiki', 999, 'article_title_3', 'Q999'),
            ],
            ['wiki_db', 'page_id', 'page_title', 'item_id']
        )
    )
    mocker.patch(
        'image_suggestions.cassandra.load_latest_revisions',
        return_value=spark_session.createDataFrame(
            [
                ('bwiki', 222, 1234567),
                ('cwiki', 111, 2345678),
                # article that is not illustrable
                ('dwiki', 111, 9876543),
            ],
            ['wiki_db', 'page_id', 'rev_id']
        )
    )
    expected = spark_session.createDataFrame(
        [
            ('bwiki', 222, 'article_title_1', 'Q2', 1234567),
            ('cwiki', 111, 'article_title_2', 'Q5', 2345678),
        ],
        ['wiki_db', 'page_id', 'page_title', 'item_id', 'rev_id']
    )
    actual = cassandra.get_illustratable_articles_with_revisions('2022-01-01')
    assert_shallow_equals(actual, expected)

def test_save_title_cache(spark_session, mocker):
    output_db = 'test'
    mocker.patch('image_suggestions.shared.save_table')
    fake_timeuuid = 'xyz'
    snapshot = '2022-05-16'
    coalesce = 2
    suggestions_full = spark_session.createDataFrame(
        [
            (
                'awiki', 100, 'awiki_page_title_100', 'Q9', fake_timeuuid, 'commons_page_1', 'commonswiki', 90,
                ['istype-depicts','istype-lead-image','istype-wikidata-image'], 12345, ['pwiki', 'qwiki'],
            ),
            (
                'bwiki', 100, 'bwiki_page_title_100', 'Q8', fake_timeuuid, 'commons_page_2', 'commonswiki', 90,
                ['istype-lead-image'], 23456, ['xwiki'],
            ),
            (
                'bwiki', 200, 'bwiki_page_title_200', 'Q7', fake_timeuuid, 'commons_page_2', 'commonswiki', 90,
                ['istype-lead-image'], 34567, ['zwiki'],
            ),
            # different suggestion for the same page - should not result in duplicate entry in title cache
            (
                'bwiki', 200, 'bwiki_page_title_200', 'Q7', fake_timeuuid, 'commons_page_3', 'commonswiki', 90,
                ['istype-wikidata-image'], 34567, [],
            ),
        ],
        [
            'wiki', 'page_id', 'page_title', 'item_id', 'id', 'image', 'origin_wiki', 'confidence',
            'kind', 'page_rev', 'found_on'
        ]
    )
    expected = spark_session.createDataFrame(
        [
            ('awiki', 100, 12345, 'awiki_page_title_100', snapshot),
            ('bwiki', 100, 23456, 'bwiki_page_title_100', snapshot),
            ('bwiki', 200, 34567, 'bwiki_page_title_200', snapshot),
        ],
        ['wiki', 'page_id', 'page_rev', 'title', 'snapshot']
    )
    actual = cassandra.save_title_cache(suggestions_full, output_db, snapshot, coalesce)
    assert_shallow_equals(actual, expected)
    shared.save_table.assert_called_with(
        actual, output_db, cassandra.TITLE_CACHE_TABLE, coalesce, partition_columns=['snapshot', 'wiki']
    )


def test_save_instanceof_cache(spark_session, mocker):
    output_db = 'test'
    mocker.patch('image_suggestions.shared.save_table')
    fake_timeuuid = 'xyz'
    snapshot = '2022-05-16'
    coalesce = 2
    suggestions_full = spark_session.createDataFrame(
        [
            (
                'awiki', 100, 'awiki_page_title_100', 'Q9', fake_timeuuid, 'commons_page_1', 'commonswiki', 90,
                ['istype-depicts','istype-lead-image','istype-wikidata-image'], 12345, ['pwiki', 'qwiki'],
            ),
            (
                'bwiki', 100, 'bwiki_page_title_100', 'Q8', fake_timeuuid, 'commons_page_2', 'commonswiki', 90,
                ['istype-lead-image'], 23456, ['xwiki'],
            ),
            # suggestion where the corresponding item_id doesn't have an instance_of value
            (
                'bwiki', 200, 'bwiki_page_title_200', 'Q7', fake_timeuuid, 'commons_page_2', 'commonswiki', 90,
                ['istype-lead-image'], 34567, ['zwiki'],
            ),
        ],
        [
            'wiki', 'page_id', 'page_title', 'item_id', 'id', 'image', 'origin_wiki', 'confidence',
            'kind', 'page_rev', 'found_on'
        ]
    )
    mocker.patch(
        'image_suggestions.cassandra.load_wikidata_items_with_P31',
        return_value=spark_session.createDataFrame(
            [
                ('Q9', 'Q111'),
                ('Q9', 'Q222'),
                ('Q8', 'Q42'),
                # item/instanceof pair without corresponding article
                ('Q3', 'Q4167410'),
            ],
            [ 'item_id', 'value' ]
        )
    )
    expected = spark_session.createDataFrame(
        [
            ('awiki', 100, 12345, ['Q111', 'Q222'], snapshot),
            ('bwiki', 100, 23456, ['Q42'], snapshot),
            ('bwiki', 200, 34567, [], snapshot),
        ],
        ['wiki', 'page_id', 'page_rev', 'instance_of', 'snapshot']
    )
    actual = cassandra.save_instanceof_cache(suggestions_full, output_db, snapshot, coalesce)
    assert_shallow_equals(actual, expected)
    shared.save_table.assert_called_with(
        actual, output_db, cassandra.INSTANCEOF_CACHE_TABLE, coalesce, partition_columns=['snapshot', 'wiki']
    )


def test_save_suggestions(spark_session, mocker):
    output_db = 'test'
    mocker.patch('image_suggestions.shared.save_table')
    fake_timeuuid = 'xyz'
    snapshot = '2022-05-16'
    coalesce = 2
    suggestions_full = spark_session.createDataFrame(
        [
            (
                'awiki', 100, 'awiki_page_title_100', 'Q9', fake_timeuuid, 'commons_page_1', 'commonswiki', 90,
                ['istype-depicts','istype-lead-image','istype-wikidata-image'], 12345, ['pwiki', 'qwiki'],
            ),
            (
                'bwiki', 100, 'bwiki_page_title_100', 'Q8', fake_timeuuid, 'commons_page_2', 'commonswiki', 90,
                ['istype-lead-image'], 23456, ['xwiki'],
            ),
            (
                'bwiki', 200, 'bwiki_page_title_200', 'Q7', fake_timeuuid, 'commons_page_2', 'commonswiki', 90,
                ['istype-lead-image'], 34567, ['zwiki'],
            ),
        ],
        [
            'wiki', 'page_id', 'page_title', 'item_id', 'id', 'image', 'origin_wiki', 'confidence',
            'kind', 'page_rev', 'found_on'
        ]
    )
    expected = spark_session.createDataFrame(
        [
            (
                'awiki', 100, fake_timeuuid, 'commons_page_1', 'commonswiki', 90, ['pwiki', 'qwiki'],
                ['istype-depicts','istype-lead-image','istype-wikidata-image'], 12345, snapshot,
            ),
            (
                'bwiki', 100, fake_timeuuid, 'commons_page_2', 'commonswiki', 90, ['xwiki'],
                ['istype-lead-image'], 23456, snapshot,
            ),
            (
                'bwiki', 200, fake_timeuuid, 'commons_page_2', 'commonswiki', 90, ['zwiki'],
                ['istype-lead-image'], 34567, snapshot,
            ),
        ],
        [
            'wiki', 'page_id', 'id', 'image', 'origin_wiki', 'confidence',
            'found_on', 'kind', 'page_rev', 'snapshot',
        ]
    )
    actual = cassandra.save_suggestions(suggestions_full, output_db, snapshot, coalesce)
    assert_shallow_equals(actual, expected)
    shared.save_table.assert_called_with(
        actual, output_db, cassandra.SUGGESTIONS_TABLE, coalesce, partition_columns=['snapshot', 'wiki']
    )


def test_prepare_depicts_data(spark_session, mocker):
    mocker.patch(
        'image_suggestions.cassandra.load_commons_images',
        return_value=spark_session.createDataFrame(
            [
                (111, 'commons_page_1'),
                (222, 'commons_page_2'),
                (333, 'commons_page_with_no_depicts'),
            ],
            ['page_id', 'page_title']
        )
    )
    mocker.patch(
        'image_suggestions.cassandra.load_commons_pages_with_depicts',
        return_value=spark_session.createDataFrame(
            [
                ('Q1', 111),
                ('Q2', 222),
                # orphaned value
                ('Q3', 999),
            ],
            ['item_id', 'page_id']
        )
    )
    expected = spark_session.createDataFrame(
        [
            ('Q1', 'commons_page_1', 70, 'istype-depicts'),
            ('Q2', 'commons_page_2', 70, 'istype-depicts'),
        ],
        ['item_id', 'page_title', 'confidence', 'kind']
    ).withColumn('found_on', F.lit(None).cast(NullType()))
    actual = cassandra.prepare_depicts_data('2022-01-01')
    assert_shallow_equals(actual, expected)

def test_prepare_wikidata_data(spark_session, mocker):
    mocker.patch(
        'image_suggestions.cassandra.load_commons_images',
        return_value=spark_session.createDataFrame(
            [
                (111, 'commons_page_1'),
                (222, 'commons_page_2'),
                (333, 'commons_page_with_no_wikidata_data'),
            ],
            ['page_id', 'page_title']
        )
    )
    mocker.patch(
        'image_suggestions.cassandra.load_wikidata_data_latest',
        return_value=spark_session.createDataFrame(
            [
                (111, 'Q1', 'image.linked.from.wikidata.p18', 1000),
                (222, 'Q2', 'image.linked.from.wikidata.p373', 200),
                # orphaned value
                (999, 'Q3', 'image.linked.from.wikidata.p373', 500),
            ],
            ['page_id', 'item_id', 'tag', 'score']
        )
    )
    expected = spark_session.createDataFrame(
        [
            ('Q1', 'commons_page_1', 90, 'istype-wikidata-image'),
            ('Q2', 'commons_page_2', 80, 'istype-commons-category'),
        ],
        ['item_id', 'page_title', 'confidence', 'kind']
    ).withColumn('found_on', F.lit(None).cast(NullType()))
    actual = cassandra.prepare_wikidata_data('hive_db', '2022-01-01')
    assert_shallow_equals(actual, expected)

def test_prepare_lead_image_data(spark_session, mocker):
    mocker.patch(
        'image_suggestions.cassandra.load_commons_images',
        return_value=spark_session.createDataFrame(
            [
                (111, 'commons_page_1'),
                (222, 'commons_page_2'),
                (333, 'commons_page_with_no_lead_image_data'),
            ],
            ['page_id', 'page_title']
        )
    )
    mocker.patch(
        'image_suggestions.cassandra.load_lead_image_data_latest',
        return_value=spark_session.createDataFrame(
            [
                (111, 'Q1', 'image.linked.from.wikipedia.lead_image', 777, ['awiki', 'bwiki']),
                (222, 'Q2', 'image.linked.from.wikipedia.lead_image', 666, ['cwiki']),
                # orphaned value
                (999, 'Q3', 'image.linked.from.wikipedia.lead_image', 500, ['dwiki']),
            ],
            ['page_id', 'item_id', 'tag', 'score', 'found_on']
        )
    )
    expected = spark_session.createDataFrame(
        [
            ('Q1', 'commons_page_1', 80, 'istype-lead-image', ['awiki', 'bwiki']),
            ('Q2', 'commons_page_2', 80, 'istype-lead-image', ['cwiki']),
        ],
        ['item_id', 'page_title', 'confidence', 'kind', 'found_on']
    )
    actual = cassandra.prepare_lead_image_data('hive_db', '2022-01-01')
    assert_shallow_equals(actual, expected)


def test_gather_suggestions_for_items(spark_session, mocker):
    mocker.patch(
        'image_suggestions.cassandra.get_all_illustrations',
        return_value=spark_session.createDataFrame(
            [
                (111, 'commons_page_1'),
                (222, 'commons_page_2'),
                (333, 'commons_page_3'),
                (444, 'commons_page_4'),
            ],
            ['page_id', 'page_title']
        )
    )
    mocker.patch(
        'image_suggestions.cassandra.prepare_lead_image_data',
        return_value=spark_session.createDataFrame(
            [
                # a) in all wikidata, lead image, depicts
                ('Q10', 'commons_page_1', 80, 'istype-lead-image', ['awiki', 'bwiki']),
                # b) in lead image and wikidata
                ('Q20', 'commons_page_2', 80, 'istype-lead-image', ['dwiki']),
                # c) in lead image and depicts
                ('Q30', 'commons_page_3', 80, 'istype-lead-image', ['dwiki','ewiki']),
                # c) in lead image only
                ('Q100', 'commons_page_1', 80, 'istype-lead-image', ['fwiki','gwiki']),
                # an image that is not an illustration, should not appear in output
                ('Q777', 'commons_page_X', 80, 'istype-lead-image', ['pwiki','qwiki']),
            ],
            ['item_id', 'page_title', 'confidence', 'kind', 'found_on']
        )
    )
    mocker.patch(
        'image_suggestions.cassandra.prepare_wikidata_data',
        return_value=spark_session.createDataFrame(
            [
                # a) in all wikidata, lead image, depicts
                ('Q10', 'commons_page_1', 90, 'istype-wikidata-image'),
                # b) in lead image and wikidata
                ('Q20', 'commons_page_2', 80, 'istype-commons-category'),
                # e) in  wikidata and depicts
                ('Q40', 'commons_page_4', 80, 'istype-commons-category'),
                # f) in wikidata only
                ('Q200', 'commons_page_2', 90, 'istype-wikidata-image'),
                # an image that is not an illustration, should not appear in output
                ('Q888', 'commons_page_Y', 80, 'istype-commons-category'),
            ],
            ['item_id', 'page_title', 'confidence', 'kind']
        ).withColumn('found_on', F.lit(None).cast(NullType()))
    )
    mocker.patch(
        'image_suggestions.cassandra.prepare_depicts_data',
        return_value=spark_session.createDataFrame(
            [
                # a) in all wikidata, lead image, depicts
                ('Q10', 'commons_page_1', 70, 'istype-depicts'),
                # c) in lead image and depicts
                ('Q30', 'commons_page_3', 70, 'istype-depicts'),
                # e) in wikidata and depicts
                ('Q40', 'commons_page_4', 70, 'istype-depicts'),
                # g) in depicts only
                ('Q300', 'commons_page_3', 70, 'istype-depicts'),
                # an image that is not an illustration, should not appear in output
                ('Q999', 'commons_page_Y', 70, 'istype-depicts'),
            ],
            ['item_id', 'page_title', 'confidence', 'kind']
        ).withColumn('found_on', F.lit(None).cast(NullType()))
    )
    # separating out the expected values with a null 'found_on' field to make the data frame easier to construct
    expected = spark_session.createDataFrame(
        [
            # a)
            ('Q10', 'commons_page_1', 90, ['istype-depicts','istype-lead-image','istype-wikidata-image'], ['awiki', 'bwiki']),
            # b)
            ('Q20', 'commons_page_2', 80, ['istype-commons-category','istype-lead-image'], ['dwiki']),
            # c)
            ('Q30', 'commons_page_3', 80, ['istype-depicts','istype-lead-image'], ['dwiki','ewiki']),
            # d)
            ('Q100', 'commons_page_1', 80, ['istype-lead-image'], ['fwiki','gwiki']),
        ],
        ['item_id', 'page_title', 'confidence', 'kind', 'found_on']
    ).union(
        spark_session.createDataFrame(
            [
                # e)
                ('Q40', 'commons_page_4', 80, ['istype-commons-category','istype-depicts']),
                # f)
                ('Q200', 'commons_page_2', 90, ['istype-wikidata-image']),
                # g)
                ('Q300', 'commons_page_3', 70, ['istype-depicts']),
            ],
            ['item_id', 'page_title', 'confidence', 'kind']
        ).withColumn('found_on', F.lit(None).cast(NullType()))
    ).orderBy('item_id', 'page_title')
    actual = cassandra.gather_suggestions_for_items('/user/test', '2022-01-01')
    assert_shallow_equals(actual, expected)

def test_generate_suggestions(spark_session, mocker):
    fake_timeuuid = 'xyz'
    snapshot = '2022-01-01'
    coalesce = 2
    mocker.patch('image_suggestions.cassandra.save_suggestions')
    mocker.patch('image_suggestions.cassandra.save_title_cache')
    mocker.patch('image_suggestions.cassandra.save_instanceof_cache')
    mocker.patch('image_suggestions.cassandra.create_dataset_id', return_value=fake_timeuuid)
    mocker.patch(
        'image_suggestions.cassandra.gather_suggestions_for_items',
        # separating out the expected values with a null 'found_on' field to make the data frame easier to construct
        return_value=spark_session.createDataFrame(
            [
                ('Q1', 'commons_page_1', 90, ['istype-depicts','istype-lead-image','istype-wikidata-image'], ['pwiki', 'qwiki']),
                # suggestion without a corresponding article in a wiki
                ('Q100', 'commons_page_100', 80, ['istype-depicts','istype-lead-image'], ['xwiki', 'ywiki']),
            ],
            ['item_id', 'page_title', 'confidence', 'kind', 'found_on']
        ).union(
            spark_session.createDataFrame(
                [
                    ('Q2', 'commons_page_2', 80, ['istype-commons-category','istype-depicts']),
                ],
                ['item_id', 'page_title', 'confidence', 'kind']
            ).withColumn('found_on', F.lit(None).cast(NullType()))
        ).orderBy('item_id', 'page_title')
    )
    mocker.patch(
        'image_suggestions.cassandra.load_suggestions_with_feedback',
        return_value=spark_session.createDataFrame(
            [
                ('bwiki', 300, 'commons_page_2'),
                # feedback for something that is not a suggestion, ought to be ignored
                ('cwiki', 111, 'commons_page_x'),
            ],
            ['wiki', 'page_id', 'filename']
        )
    )
    mocker.patch(
        'image_suggestions.cassandra.get_illustratable_articles_with_revisions',
        return_value=spark_session.createDataFrame(
            [
                ('awiki', 100, 'awiki_page_title_100', 'Q1', 12345),
                ('awiki', 101, 'awiki_page_title_101', 'Q2', 23456),
                ('bwiki', 200, 'bwiki_page_title_200', 'Q2', 34567),
                # article without suggestion
                ('cwiki', 111, 'cwiki_page_title_111', 'Q3', 45678),
                # the suggestion for this article has been rejected or accepted already, should be absent from results
                ('bwiki', 300, 'bwiki_page_title_300', 'Q2', 56789),
            ],
            ['wiki_db', 'page_id', 'page_title', 'item_id', 'rev_id']
        )
    )
    # separating out the expected values with a null 'found_on' field to make the data frame easier to construct
    expected = spark_session.createDataFrame(
        [
            (
                'awiki', 100, 'awiki_page_title_100', 'Q1', fake_timeuuid, 'commons_page_1', 'commonswiki',
                90, ['pwiki', 'qwiki'], ['istype-depicts','istype-lead-image','istype-wikidata-image'], 12345,
            ),
        ],
        [
            'wiki', 'page_id', 'page_title', 'item_id', 'id', 'image', 'origin_wiki',
            'confidence', 'found_on', 'kind', 'page_rev'
        ]
    ).union(
        spark_session.createDataFrame(
            [
                (
                    'awiki', 101, 'awiki_page_title_101', 'Q2', fake_timeuuid, 'commons_page_2', 'commonswiki',
                    80, ['istype-commons-category','istype-depicts'], 23456
                ),
                (
                    'bwiki', 200, 'bwiki_page_title_200', 'Q2', fake_timeuuid, 'commons_page_2', 'commonswiki',
                    80, ['istype-commons-category','istype-depicts'], 34567
                ),
            ],
            [
                'wiki', 'page_id', 'page_title', 'item_id', 'id', 'image', 'origin_wiki',
                'confidence', 'kind', 'page_rev',
            ]
        ).withColumn(
            'found_on',
            F.lit(None).cast(NullType())
        ).select(
            'wiki', 'page_id', 'page_title', 'item_id', 'id', 'image', 'origin_wiki',
            'confidence', 'found_on', 'kind', 'page_rev',
        )
    )
    actual = cassandra.generate_suggestions('test', snapshot, coalesce)
    assert_shallow_equals(actual, expected)
    cassandra.save_suggestions.assert_called_with(actual, 'test', snapshot, coalesce)
    cassandra.save_title_cache.assert_called_with(actual, 'test', snapshot, coalesce)
    cassandra.save_instanceof_cache.assert_called_with(actual, 'test', snapshot, coalesce)
