# Image suggestions
Spark jobs for the [image suggestions data pipeline](https://www.mediawiki.org/wiki/Structured_Data_Across_Wikimedia/Image_Suggestions/Data_Pipeline).

## Release workflow
We follow Data Engineering's [workflow_utils](https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils/-/blob/main/gitlab_ci_templates/README.md#project-versioning):
- the `main` branch is on a `.dev` release
- releases are made by removing the `.dev` suffix and committing a tag

## How to deploy
1. On the left sidebar, go to **CI/CD > Pipelines**
2. Click on the _play_ button and select `trigger_release`. Wait until done. The job should create a git tag as well as a [new artifact in the Package Registry](https://gitlab.wikimedia.org/repos/structured-data/image-suggestions/-/packages). Confirm that is the case.
3. Now click on the _play_ button again but this time select `bump_on_airflow_dags`. This will create a merge request over at the [airflow-dags](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags) repo.
7. Inspect the merge request over at `airflow-dags`, and then merge it. You can also add commits to it with futher changes as needed.
8. Deploy the DAGs:
```sh
me@my_box:~$ ssh deployment.eqiad.wmnet
me@deploy1002:~$ cd /srv/deployment/airflow-dags/platform_eng/
me@deploy1002:~$ git pull
me@deploy1002:~$ scap deploy
```
See the [docs](https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils/-/blob/main/gitlab_ci_templates/README.md#example-pipeline-usage) for more details.
