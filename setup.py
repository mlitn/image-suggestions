from setuptools import find_packages, setup

setup(
    name='image-suggestions',
    packages=find_packages(),
    version='0.8.0.dev',
    description='pyspark jobs for image-suggestions',
    author='mfossati',
    entry_points={
        'console_scripts': [
            'cassandra.py = image_suggestions.cassandra:main',
            'commonswiki_file.py = image_suggestions.commonswiki_file:main',
            'search_indices.py = image_suggestions.search_indices:main',
        ]
    }
)
