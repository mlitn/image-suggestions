#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Query the Analytics data lake and gather a dataset with weighted tags from Wikidata and Wikipedias
for the Commons search index. See https://phabricator.wikimedia.org/T286562, https://phabricator.wikimedia.org/T299408"""

import argparse

from fsspec import filesystem  # type: ignore
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from image_suggestions import queries, shared

FILE_NAMESPACE_VALUE = 6
P18_TAG = 'image.linked.from.wikidata.p18'
P373_TAG = 'image.linked.from.wikidata.p373'
LEAD_IMAGE_TAG = 'image.linked.from.wikipedia.lead_image'
MAX_SCORE = 1000


# BEGIN: utilities
def save_wikidata_data(wikidata_data: DataFrame, hive_db: str, snapshot: str,
                       coalesce: int) -> DataFrame:  # pragma: no cover
    shared.save_table(
        wikidata_data.withColumn('snapshot', F.lit(snapshot)),
        hive_db, shared.WIKIDATA_DATA,
        coalesce
    )

    return wikidata_data


def save_lead_image_data(lead_image_data: DataFrame, hive_db: str, snapshot: str,
                         coalesce: int) -> DataFrame:  # pragma: no cover
    shared.save_table(
        lead_image_data.withColumn('snapshot', F.lit(snapshot)),
        hive_db, shared.LEAD_IMAGE_DATA,
        coalesce
    )

    return lead_image_data


def parse_args() -> argparse.Namespace:  # pragma: no cover
    parser = argparse.ArgumentParser(
        description='Gather a dataset with Wikidata/Wikipedia weighted tags for the Commons search index'
    )
    parser.add_argument('hive_db', help='Hive DB for output')
    parser.add_argument('snapshot', help='Snapshot date (YYYY-MM-DD)')
    parser.add_argument('coalesce', help='Controls the amount of files generated per Hive partition',
                        type=int, default=2)

    return parser.parse_args()
# END: utilities


# BEGIN: initial queries against the data lake
# TODO `spark` is an outer-score variable: pass it as an arg to all the functions below
def load_wikidata_items_with_P18(snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(
        queries.wikidata_items_with_P18.format(snapshot)
    )


def load_wikidata_items_with_P373(snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(
        queries.wikidata_items_with_P373.format(snapshot)
    )


def load_commons_file_pages(short_snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(
        queries.commons_file_pages.format(short_snapshot)
    )


def load_categories(short_snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(
        queries.categories.format(short_snapshot)
    )


def load_category_links(short_snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(
        queries.category_links.format(short_snapshot)
    )


def load_non_commons_main_pages(short_snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(
        queries.non_commons_main_pages.format(short_snapshot)
    )


def load_pagelinks(short_snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(
        queries.pagelinks.format(short_snapshot)
    )


def load_wikidata_item_page_links(snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(
        queries.wikidata_item_page_links.format(snapshot)
    )


def load_lead_images(short_snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(
        queries.pages_with_lead_images.format(short_snapshot)
    )
# END: initial queries aginst the data lake


def gather_wikidata_data(
    commons_file_pages: DataFrame, wikidata_items_with_P18: DataFrame, wikidata_items_with_P373: DataFrame,
    snapshot: str, hive_db: str, coalesce: int
) -> DataFrame:
    short_snapshot = shared.get_monthly_snapshot(snapshot)
    commons_with_reverse_p18 = gather_commons_with_reverse_p18(
        commons_file_pages, wikidata_items_with_P18
    )
    commons_with_reverse_p373 = gather_commons_with_reverse_p373(
        commons_file_pages, wikidata_items_with_P373, short_snapshot
    )
    wikidata_data = commons_with_reverse_p18.union(commons_with_reverse_p373)

    return save_wikidata_data(wikidata_data, hive_db, snapshot, coalesce)


def gather_commons_with_reverse_p18(commons_file_pages: DataFrame, wikidata_items_with_P18: DataFrame) -> DataFrame:
    return (
        commons_file_pages
        .withColumn(shared.TAG_COLNAME, F.lit(P18_TAG))
        # set a constant score for P18 values to Elasticsearch's maximum, otherwise it defaults to 1
        .withColumn(shared.SCORE_COLNAME, F.lit(MAX_SCORE))
        .join(
            wikidata_items_with_P18,
            on=[commons_file_pages.page_title == wikidata_items_with_P18.value],
            how='inner'
        )
        .select(
            commons_file_pages.page_id, wikidata_items_with_P18.item_id, shared.TAG_COLNAME, shared.SCORE_COLNAME
        )
    )


def gather_categories_with_links(short_snapshot: str) -> DataFrame:
    categories = load_categories(short_snapshot)
    category_links = load_category_links(short_snapshot)

    return categories.join(category_links, on=['cat_title'], how='inner')

def gather_commons_with_reverse_p373(
    commons_file_pages: DataFrame, wikidata_items_with_P373: DataFrame, short_snapshot: str
) -> DataFrame:
    all_commons_with_reverse_p373 = gather_all_commons_with_reverse_p373(
        commons_file_pages,
        wikidata_items_with_P373,
        short_snapshot
    )
    return filter_commons_with_reverse_p373(all_commons_with_reverse_p373)

def gather_all_commons_with_reverse_p373(
    commons_file_pages: DataFrame, wikidata_items_with_P373: DataFrame, short_snapshot: str
) -> DataFrame:
    cats_with_links = gather_categories_with_links(short_snapshot)
    files_with_categories = commons_file_pages.join(
        cats_with_links,
        on=[commons_file_pages.page_id == cats_with_links.page_id],
        how='inner'
    )

    return (
        files_with_categories
        .withColumn(shared.TAG_COLNAME, F.lit(P373_TAG))
        .join(
            wikidata_items_with_P373,
            on=[cats_with_links.cat_title == wikidata_items_with_P373.value],
            how='inner'
        )
        .withColumn(
            shared.SCORE_COLNAME,
            # If an image is one of 5 in a category, then we care more about that category than one with 10k images
            # Calculate a score that's inversely proportional to the log of number of images in the category
            F.round(
                (1 / F.log(files_with_categories.cat_pages + 1)) * MAX_SCORE / 1.443
            ).cast('int')
        )
        .select(
            commons_file_pages.page_id, wikidata_items_with_P373.item_id, shared.TAG_COLNAME, shared.SCORE_COLNAME
        )
    )


def filter_commons_with_reverse_p373(all_commons_with_reverse_p373: DataFrame) -> DataFrame:
    # If a commons file has its own wikidata item (via P18), then on wikidata its commons category is often (or
    # always?) used to indicate the category the wikidata item is *in* rather than the category containing images
    # *relevant to* the wikidata item
    #
    # This sense of the use of commons category is not useful for search - if I'm searching for an image for "Christ
    # washing the feet of the apostles" I don't want other images from Category:Images_from_the_Royal_Library_of_Belgium
    # in the results. Similarly for image suggestions other images from that category are not appropriate suggestions
    # for an article about that particular artwork.
    #
    # It's impossible to tell definitively which meaning of the P373 property is being used. The best guess we can
    # make at the moment is that if an image is in a commons category that is the P373 value for >99 different wikidata
    # items, then the P373 value is probably being used in the sense of "the category the wikidata item is in" rather
    # than "the category containing images related to the wikidata item", and therefore we should ignore it.
    #
    # See https://phabricator.wikimedia.org/T314120
    images_to_filter = (
            all_commons_with_reverse_p373
            .select('item_id', 'page_id')
            .groupBy('page_id')
            .agg(F.count('item_id').alias('wikidata_item_count'))
            .filter(F.col('wikidata_item_count')>99)
            .select('page_id')
        )

    return (
        all_commons_with_reverse_p373
        .join(
            images_to_filter,
            on=['page_id'],
            how='left_anti'
        )
    )

def gather_articles_with_lead_images(snapshot: str, short_snapshot: str) -> DataFrame:
    return filter_articles_with_lead_images(
        gather_all_articles_with_lead_images(snapshot, short_snapshot)
    )

def gather_all_articles_with_lead_images(snapshot: str, short_snapshot: str) -> DataFrame:
    commons_file_pages = load_commons_file_pages(short_snapshot)
    pages_with_lead_images = load_lead_images(short_snapshot)
    non_commons_main_pages = load_non_commons_main_pages(short_snapshot)
    wikidata_item_page_links = load_wikidata_item_page_links(snapshot)

    return (
        commons_file_pages
        .join(
            pages_with_lead_images,
            on=[pages_with_lead_images.lead_image_title == commons_file_pages.page_title],
            how='inner'
        )
        # Need to join the `page` table to get the title of the page containing the lead image
        .join(
            non_commons_main_pages,
            on=[
                pages_with_lead_images.page_id == non_commons_main_pages.page_id,
                pages_with_lead_images.wiki_db == non_commons_main_pages.wiki_db,
            ],
            how='inner'
        )
        .join(
            wikidata_item_page_links,
            on=[
                non_commons_main_pages.page_id == wikidata_item_page_links.page_id,
                non_commons_main_pages.wiki_db == wikidata_item_page_links.wiki_db,
            ],
            how='inner'
        )
        .select(
            pages_with_lead_images.wiki_db.alias('article_wiki'),
            non_commons_main_pages.page_title.alias('article_title'),
            wikidata_item_page_links.item_id,
            commons_file_pages.page_id.alias('commons_page_id'),
        )
    )

def filter_articles_with_lead_images(all_articles_with_lead_images: DataFrame) -> DataFrame:
    # Some images are used as the lead image on tens or even hundreds of thousands of pages - for example
    # there is an svg USA map that's used as lead image on >400k wiki articles about geographical districts in the US.
    #
    # This is not actually useful for users - I probably don't want a USA map as result in an image search
    # for "Sowbelly ridge" - and also clogs up the commons search index doc for the image with thousands of
    # `weighted_tags` entries containing the wikidata id of each wiki article.
    #
    # To solve this and prevent the commonswiki_file index from being overwhelmed we'll filter out rows from this
    # dataframe that have >99 wikidata ids for a single image.
    #
    # In practical terms this means that if an image is a lead image for a set of articles N, and if the set N contains
    # articles with >99 distinct wikidata ids, then the image is likely to be too general to be useful as a search
    # result or an image suggestion, and so we ignore it.
    #
    # See https://phabricator.wikimedia.org/T314120
    images_to_filter = (
        all_articles_with_lead_images
        .select('item_id', 'commons_page_id')
        .groupBy('commons_page_id')
        .agg(F.count('item_id').alias('wikidata_item_count'))
        .filter(F.col('wikidata_item_count')>99)
        .select('commons_page_id')
    )

    return (
        all_articles_with_lead_images
        .join(
            images_to_filter,
            on=['commons_page_id'],
            how='left_anti'
        )
    )


def add_link_counts(short_snapshot: str, articles_with_lead_images: DataFrame) -> DataFrame:
    pagelinks = load_pagelinks(short_snapshot)

    link_counts_by_wiki = (
        articles_with_lead_images
        .join(
            pagelinks,
            on=[
                articles_with_lead_images.article_title == pagelinks.to_title,
                articles_with_lead_images.article_wiki == pagelinks.wiki_db,
            ],
            how='inner'
        )
        .groupBy('article_wiki', 'article_title', 'item_id', 'commons_page_id')
        .agg(F.count(pagelinks.from_id).alias('incoming_link_count'))
    )

    return (
        link_counts_by_wiki
        .groupBy('item_id', 'commons_page_id')
        .agg(
            F.sum(link_counts_by_wiki.incoming_link_count).alias('incoming_link_count_all'),
            F.collect_set(link_counts_by_wiki.article_wiki).alias(shared.FOUND_ON_COLNAME)
        )
        .select(
            'commons_page_id', 'item_id', 'incoming_link_count_all', shared.FOUND_ON_COLNAME
        )
    )


def gather_lead_image_data(snapshot: str, hive_db: str, coalesce: int) -> DataFrame:
    short_snapshot = shared.get_monthly_snapshot(snapshot)
    articles_with_lead_images = gather_articles_with_lead_images(snapshot, short_snapshot)
    with_link_count = add_link_counts(short_snapshot, articles_with_lead_images)

    lead_image_data = (
        with_link_count
        .groupBy(
            'commons_page_id', 'item_id', 'incoming_link_count_all', shared.FOUND_ON_COLNAME
        )
        .agg(
            # We want to give lead images an importance score based on the number of main namespace pages that link
            # to articles with that lead image, grouped by the wikidata id of the article
            # ... so for example if Commons_File_A is a lead image on xxwiki/Article_X and yywiki/Article_Y which
            #    both correspond to wikidata item Q123, then the score for Commons_File_A as an image suggestion
            #    for Q123 is proportional to the sum of incoming links for Article_X and Article_Y
            # We're arbitrarily choosing 0.2 as a scaling factor for incoming links, which means that
            # - if Article_X on xxwiki and Article_Y on yywiki correspond to wikidata id Q123
            # - and the lead image for both Article_X and Article_Y is Commons_File_A
            # - and Article_X has 4000 incoming links, and Article_Y has 1000 incoming links
            # - we'll store Q123 in the commonswiki_file document for Commons_File_A with a score of 1000
            # If Article_X and Article_Y together have LESS then 5000 links, we'll store a score of
            #    incoming_link_count * 0.2
            # If Article_X and Article_Y together have MORE than 5000 links, we'll still store a score of 1000
            #    (because that's the max score we can store for weighted tags in elasticsearch)
            F.when(
                with_link_count.incoming_link_count_all * F.lit(0.2) > MAX_SCORE, MAX_SCORE
            )
            .otherwise(
                F.ceil(with_link_count.incoming_link_count_all * F.lit(0.2)).cast('int')
            )
            .alias(shared.SCORE_COLNAME)
        )
        .withColumn(shared.TAG_COLNAME, F.lit(LEAD_IMAGE_TAG))
        .select(
            with_link_count.commons_page_id.alias('page_id'),
            with_link_count.item_id,
            shared.TAG_COLNAME,
            shared.SCORE_COLNAME,
            shared.FOUND_ON_COLNAME
        )
    )

    return save_lead_image_data(lead_image_data, hive_db, snapshot, coalesce)


def get_commonswiki_file_data(wd_data: DataFrame, li_data: DataFrame) -> DataFrame:
    return (
        wd_data
        .withColumn('value', F.concat_ws('|', wd_data.item_id, wd_data.score))
        .select('page_id', shared.TAG_COLNAME, 'item_id', shared.SCORE_COLNAME, 'value')
        .union(
            li_data
            .withColumn('value', F.concat_ws('|', li_data.item_id, li_data.score))
            .select(
                'page_id', shared.TAG_COLNAME, 'item_id', shared.SCORE_COLNAME, 'value'
            )
        )
        .orderBy('page_id', shared.TAG_COLNAME, 'value')
        .groupBy('page_id', shared.TAG_COLNAME)
        # `collect_set` doesn't preserve order, so use `collect_list` with `array_distinct` instead
        .agg(
            F.array_distinct(F.collect_list('value'))
            .alias(shared.VALUES_COLNAME)
        )
        .withColumn(shared.WIKIID_COLNAME, F.lit(shared.COMMONSWIKI_VALUE))
        .withColumn(shared.PAGE_NAMESPACE_COLNAME, F.lit(FILE_NAMESPACE_VALUE))
        .select(
            shared.WIKIID_COLNAME, shared.PAGE_NAMESPACE_COLNAME, 'page_id', shared.TAG_COLNAME, shared.VALUES_COLNAME
        )
        .orderBy(
            shared.WIKIID_COLNAME, shared.PAGE_NAMESPACE_COLNAME, 'page_id', shared.TAG_COLNAME
        )
    )


def main(args: argparse.Namespace) -> None:
    hive_db = args.hive_db
    snapshot = args.snapshot
    coalesce = args.coalesce

    short_snapshot = shared.get_monthly_snapshot(snapshot)

    commons_file_pages = load_commons_file_pages(short_snapshot)
    wikidata_items_with_P18 = load_wikidata_items_with_P18(snapshot)
    wikidata_items_with_P373 = load_wikidata_items_with_P373(snapshot)
    wd_data = gather_wikidata_data(
        commons_file_pages, wikidata_items_with_P18, wikidata_items_with_P373, snapshot, hive_db, coalesce
    )
    li_data = gather_lead_image_data(snapshot, hive_db, coalesce)

    commonswiki_file_current = get_commonswiki_file_data(wd_data, li_data)
    shared.write_search_index_full(commonswiki_file_current, hive_db, snapshot, coalesce)


if __name__ == '__main__':
    args = parse_args()

    # Prod
    spark = SparkSession.builder.getOrCreate()
    # Dev
    # from wmfdata.spark import create_custom_session  # type: ignore
    # spark = create_custom_session(
        # master='yarn',
        # app_name='image-suggestions-dev',
        # # See `modified_args` in
        # # https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/platform_eng/dags/image-suggestions_dag.py
        # spark_config={
            # 'spark.driver.cores': 2,
            # 'spark.driver.memory': '12G',
            # 'spark.executor.cores': 4,
            # 'spark.executor.memory': '9G',
            # 'spark.dynamicAllocation.enabled': 'true',
            # 'spark.dynamicAllocation.maxExecutors': '64',
            # 'spark.shuffle.service.enabled': 'true',
            # 'spark.sql.shuffle.partitions': 256,
            # 'spark.sql.sources.partitionOverwriteMode': 'dynamic',
        # }
    # )
    main(args)
    spark.stop()
