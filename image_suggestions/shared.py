#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Shared code used by *.py scripts that read from the data lake for image suggestions"""

from datetime import datetime, timedelta
from typing import Optional

from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from image_suggestions import queries

WIKIID_COLNAME = 'wikiid'
PAGE_NAMESPACE_COLNAME = 'page_namespace'
TAG_COLNAME = 'tag'
SCORE_COLNAME = 'score'
FOUND_ON_COLNAME = 'found_on'
VALUES_COLNAME = 'values'
COMMONSWIKI_VALUE = 'commonswiki'
SEARCH_INDEX_FULL_TABLE_NAME = 'image_suggestions_search_index_full'
SEARCH_INDEX_DELTA_TABLE_NAME = 'image_suggestions_search_index_delta'

# table names for storing intermediate data for use across scripts
WIKIDATA_DATA = 'image_suggestions_wikidata_data'
LEAD_IMAGE_DATA = 'image_suggestions_lead_image_data'


# Return the most recent monthly snapshot corresponding to a weekly snapshot
# Note that the date of a snapshot is the date of the *beginning* of the snapshot period, so:
#   - a weekly snapshot 2022-05-16 (a Monday) covers data up until 2022-05-22T23:59:59 (end of the following Sunday)
#   - a monthly snapshot 2022-05 covers data up until 2022-05-31T23:59:59
# So the monthly snapshot corresponding to a weekly snapshot is the month part of the last day of the week that
# corresponding to the weekly snapshot, minus 1
# Example 1:
#   weekly snapshot == 2022-05-16
#   Covers data up until 2022-05-22T23:59:59
#   At that time the most recent monthly snapshot is the one generated at the end of April, so the monthly
#   snapshot is 2022-04
# Example 2:
#   weekly snapshot == 2022-05-30
#   Covers data up until 2022-06-05T23:59:59
#   At that time the most recent monthly snapshot is the one generated at the end of May, so the monthly
#   snapshot is 2022-05
def get_monthly_snapshot(snapshot: str) -> str:
    input_format = '%Y-%m-%d'  # YYYY-MM-DD
    output_format = '%Y-%m'  # YYYY-MM
    try:
        weekly_snapshot_date = datetime.strptime(snapshot, input_format)
        end_of_week = weekly_snapshot_date - timedelta(days=weekly_snapshot_date.weekday()) + timedelta(days=6)
        previous_month = end_of_week.replace(day=1) - timedelta(days=1)
    except ValueError:
        # Just a more eloquent error message
        raise ValueError(f'Invalid snapshot, must be YYYY-MM-DD: {snapshot}')

    return previous_month.strftime(output_format)


# See https://phabricator.wikimedia.org/T302095
def compute_search_index_delta(previous: DataFrame, current: DataFrame) -> DataFrame:
    previous = previous.orderBy(WIKIID_COLNAME, PAGE_NAMESPACE_COLNAME, 'page_id', TAG_COLNAME, VALUES_COLNAME)
    current = current.orderBy(WIKIID_COLNAME, PAGE_NAMESPACE_COLNAME, 'page_id', TAG_COLNAME, VALUES_COLNAME)
    changed = (
        current
        .subtract(previous)
        .select(WIKIID_COLNAME, PAGE_NAMESPACE_COLNAME, 'page_id', TAG_COLNAME, VALUES_COLNAME)
    )
    deleted = (
        previous
        .join(current, on=[WIKIID_COLNAME, PAGE_NAMESPACE_COLNAME, 'page_id', TAG_COLNAME], how='left_anti')
        .withColumn('values', F.array(F.lit('__DELETE_GROUPING__')))
        .select(WIKIID_COLNAME, PAGE_NAMESPACE_COLNAME, 'page_id', TAG_COLNAME, VALUES_COLNAME)
    )

    return (
        changed
        .union(deleted)
        .orderBy(WIKIID_COLNAME, PAGE_NAMESPACE_COLNAME, 'page_id', TAG_COLNAME, VALUES_COLNAME)
    )


def get_data_from_previous_snapshot(spark_session: SparkSession, hive_db: str) -> DataFrame:
    return spark_session.sql(
        queries.search_index.format(
            hive_db, SEARCH_INDEX_FULL_TABLE_NAME,
            hive_db, SEARCH_INDEX_FULL_TABLE_NAME
        )
    )


def write_search_index_full(data: DataFrame, hive_db: str, snapshot: str, coalesce: int) -> DataFrame:
    save_table(
        data.withColumn('snapshot', F.lit(snapshot)), hive_db, SEARCH_INDEX_FULL_TABLE_NAME, coalesce
    )

    return data


def write_search_index_delta(
    data: DataFrame, hive_db: str, snapshot: str, coalesce: int, spark_session: SparkSession
) -> None:
    previous = get_data_from_previous_snapshot(spark_session, hive_db)
    delta = compute_search_index_delta(previous, data)

    save_table(
        delta.withColumn('snapshot', F.lit(snapshot)), hive_db, SEARCH_INDEX_DELTA_TABLE_NAME, coalesce
    )


def save_table(
    data: DataFrame, db_name: str, table_name: str, coalesce: int,
    partition_columns: list = ['snapshot'], mode: str = 'append'
) -> None:
    # write what's happening to the spark context so we can monitor it
    target = f'{db_name}.{table_name}'
    partition = ",".join(partition_columns)
    data.rdd.context.setLocalProperty(
        'callSite.short', f'{mode} "{target}" table, partition by "{partition}"'
    )

    data.coalesce(coalesce).write.saveAsTable(
        target,
        partitionBy=partition_columns,
        mode=mode
    )

